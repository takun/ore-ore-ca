# ore-ore-ca (オレオレ認証局)

Environment: Ubuntu 20.04 LTS with python-is-python3 package

```
$ chmod +x create-ca.sh
$ ./create-ca.sh
$ chmod +x issue-certificate.sh
$ ./issue-certificate.sh
$ cp server/server.crt https-test  # the server certificate (including servers public key)
$ cp server/server.key https-test  # the private key of the server
$ cd https-test
$ chmod +x server.py
$ ./server.py
```

## Register Your Certificate to Your Browsers

Use ore-ore-ca/ca.crt that is the certificate authoritys certificate.

[自分で発行した認証局証明書をブラウザに登録する方法](https://kekaku.addisteria.com/wp/20190327103825)

## Open https://localhost:8000/ with Your Browsers

Check if you see a hello message.

## Stop Your Server with Ctrl + C
