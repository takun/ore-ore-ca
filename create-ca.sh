#!/bin/bash

# shellcheck

set -o errexit   # exit when an error occurs
set -o pipefail  # detect an error in pipes
set -o nounset   # deny undefined variables

CA=ore-ore-ca

mkdir -p $CA           # not try to create a new directory when it is there because of -p
mkdir -p $CA/certs     # certificate of certificate authority
mkdir -p $CA/private   # private key of certificate authority
mkdir -p $CA/crl       # certificate revocation list
mkdir -p $CA/newcerts  # new issued certificates

#
# certificates management files
#

echo 01 > $CA/serial             # serial number of certificates
echo 01 > $CA/crlnumber
cp /dev/null $CA/index.txt       # index of certificates
cp /dev/null $CA/index.txt.attr

#
# create certificate authority
#

export OPENSSL_CONF=ore-ore-ca.cnf  # based on /etc/ssl/openssl.cnf

openssl genrsa -out $CA/private/ca.key                                           # private key

# certificate signing request
openssl req -new -key $CA/private/ca.key -out $CA/ca.csr

# selfsign:   root certificate authority
# extensions: use a following section of $OPENSSL_CONF
# days:       not more than 825
openssl ca -in $CA/ca.csr -out $CA/ca.crt -selfsign -extensions v3_ca -days 825  # certificate
