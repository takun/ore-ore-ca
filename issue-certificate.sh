#!/bin/bash

# shellcheck

set -o errexit   # exit when an error occurs
set -o pipefail  # detect an error in pipes
set -o nounset   # deny undefined variables

DIR=server
NAME=server

mkdir -p $DIR  # not try to create a new directory when it is there because of -p

cat << TXT > $DIR/ext-v3
basicConstraints=CA:FALSE
nsComment="OpenSSL Generated Certificate"
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer
subjectAltName=DNS:localhost,IP:127.0.0.1
TXT

export OPENSSL_CONF=ore-ore.cnf

openssl genrsa -out $DIR/$NAME.key  # private key

# certificate signing request
openssl req -new -key $DIR/$NAME.key -out $DIR/$NAME.csr

# days:    not more than 825
# extfile: must be the last option
openssl ca -in $DIR/$NAME.csr -out $DIR/$NAME.crt -days 825 -extfile $DIR/ext-v3  # certificate
